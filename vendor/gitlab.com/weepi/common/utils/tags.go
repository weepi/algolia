package utils

import (
	"reflect"
)

// Returns spefified struct tags name
func GetStructTags(s interface{}, n string) []string {
	v := reflect.ValueOf(s)
	numFields := v.NumField()
	var out []string
	for i := 0; i < numFields; i++ {
		tag := v.Type().Field(i).Tag.Get(n)
		if tag == "" || tag == "-" {
			continue
		}
		out = append(out, tag)
	}
	return out
}
