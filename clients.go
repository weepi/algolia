package main

import (
	"fmt"

	"github.com/algolia/algoliasearch-client-go/algoliasearch"
	"github.com/nats-io/go-nats"
	"googlemaps.github.io/maps"
)

type clients struct {
	NatsClient    *nats.EncodedConn
	AlgoliaClient algoliasearch.Client
	MapsClient    *maps.Client
}

func (c *clients) InitNats() {
	dsn := fmt.Sprintf("nats://%s", Cfg.NATSAddr)
	nc, err := nats.Connect(dsn)
	if err != nil {
		Log.With("error", err).Fatal("can't connect to NATS")
	}
	econn, err := nats.NewEncodedConn(nc, nats.JSON_ENCODER)
	if err != nil {
		Log.With("error", err).Fatal("can't create encoded conn")
	}

	c.NatsClient = econn
}

func (c *clients) InitAlgolia() {
	c.AlgoliaClient = algoliasearch.NewClient(Cfg.AlgoliaAppId, Cfg.AlgoliaApiKey)
}

func (c *clients) InitMaps() {
	mapsClient, err := maps.NewClient(maps.WithAPIKey(Cfg.MapsApiKey))
	if err != nil {
		Log.With("error", err).Fatal("can not initiate google client")
	}

	c.MapsClient = mapsClient
}

func initClients() clients {
	clients := clients{}

	clients.InitNats()
	clients.InitAlgolia()
	clients.InitMaps()

	return clients
}
