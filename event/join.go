package event

import (
	"github.com/algolia/algoliasearch-client-go/algoliasearch"
)

func Join(uuid string, algoliaIndex algoliasearch.Index) error {
	object, err := algoliaIndex.GetObject(uuid, []string{"objectID", "nbParticipants"})
	if err != nil {
		return err
	}

	object["nbParticipants"] = object["nbParticipants"].(float64) + 1

	if _, err := algoliaIndex.PartialUpdateObject(object); err != nil {
		return err
	}

	return nil
}
