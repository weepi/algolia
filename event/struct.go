package event

import "time"

const (
	A = iota
)

type Coords struct {
	Lat float64 `structs:"lat"`
	Lng float64 `structs:"lng"`
}

type Reservation struct {
	Price    float64 `structs:"price"`
	Currency string  `structs:"currency"`
}

type Location struct {
	Geoloc  Coords `structs:"_geoloc"`
	City    string `structs:"city"`
	Zip     uint64 `structs:"zip"`
	Country string `structs:"country"`
}

type EventMsg struct {
	ObjectID         string      `structs:"objectID"`
	Title            string      `structs:"title"`
	Private          bool        `structs:"private"`
	Sponsored        bool        `structs:"sponsored"`
	StartDate        time.Time   `structs:"startDate"`
	EndDate          time.Time   `structs:"endDate"`
	EndDateTimestamp uint64      `structs:"endDateTimestamp"`
	Description      string      `structs:"description"`
	Geoloc           Coords      `json:"_geoloc" structs:"_geoloc"`
	Location         Location    `structs:"location"`
	SportUUID        string      `structs:"sportUUID"`
	Sport            string      `structs:"sport"`
	MinParticipants  uint32      `structs:"minParticipants"`
	MaxParticipants  uint32      `structs:"maxParticipants"`
	NbParticipants   uint32      `structs:"nbParticipants"`
	OwnerUUID        string      `structs:"ownerUUID"`
	Reservation      Reservation `structs:"reservation"`
}
