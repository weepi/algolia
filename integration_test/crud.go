package main

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"gitlab.com/weepi/common/utils"

	"github.com/algolia/algoliasearch-client-go/algoliasearch"
	"github.com/caarlos0/env"
	"github.com/joho/godotenv"
)

type config struct {
	WeepiEventAddr string `env:"WEEPI_EVENT_ADDR"`
	Env            string `env:"ENV"`
	AlgoliaAppId   string `env:"ALGOLIA_APP_ID"`
	AlgoliaApiKey  string `env:"ALGOLIA_API_KEY"`
}

const (
	configFilename = "config"
	tagName        = "env"
)

func init() {
	var helpFlag = flag.Bool("help", false, "display help")
	flag.Parse()
	if *helpFlag {
		help()
		os.Exit(0)
	}
}

var (
	Cfg config
)

func help() {
	fmt.Println("The following env vars can be set:")
	for _, v := range utils.GetStructTags(Cfg, tagName) {
		fmt.Printf("%s\n", v)
	}
}

func compareEvents(jsonObject []byte, algoliaObject algoliasearch.Object) error {
	var sentObjectmap map[string]interface{}
	json.Unmarshal(jsonObject, &sentObjectmap)

	for key, value := range sentObjectmap {
		if key == "latitude" {
			if algoliaObject["_geoloc"].(map[string]interface{})["lat"].(float64) != value {
				return errors.New(fmt.Sprintf("%s differs", key))
			}
		} else if key == "longitude" {
			if algoliaObject["_geoloc"].(map[string]interface{})["lng"].(float64) != value {
				return errors.New(fmt.Sprintf("%s differs", key))
			}
		} else if key == "UUID" {
			if algoliaObject["objectID"].(string) != value {
				return errors.New(fmt.Sprintf("UUID differs"))
			}
		} else if key != "sport" && value != algoliaObject[key] {
			return errors.New(fmt.Sprintf("%s differs", key))
		}
	}
	return nil
}

func main() {
	// Load env vars from file
	if err := godotenv.Load(configFilename); err != nil {
		panic("could not read configuration from file")
	}
	// Get config from env vars
	if err := env.Parse(&Cfg); err != nil {
		panic("could not parse configuration")
	}

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	httpClient := &http.Client{Transport: tr}

	algoliaClient := algoliasearch.NewClient(Cfg.AlgoliaAppId, Cfg.AlgoliaApiKey)
	index := algoliaClient.InitIndex(Cfg.Env + "_events")

	//CREATE
	var jsonStr = []byte(`{
		"private": false,
		"sponsored": true,
		"title": "Velo a Tomsk",
		"startDate": "2018-04-16T06:10:18Z",
		"endDate": "2018-04-16T06:11:18Z",
		"description": "Course de velo autour de la ville de Tomsk",
		"latitude": 56.451465,
		"longitude": 84.977011,
		"sport": "86ff7f06-5192-4367-bbda-43e690453f6f",
		"minParticipants": 3,
		"maxParticipants": 42,
		"ownerUUID": "56af7f06-5192-4367-bbda-43e660463f6f"
	}`)
	req, err := http.NewRequest("POST", Cfg.WeepiEventAddr, bytes.NewBuffer(jsonStr))
	if err != nil {
		panic(err)
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := httpClient.Do(req)
	if err != nil {
		panic(err)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	var raw map[string]interface{}

	json.Unmarshal(body, &raw)

	uuid := raw["UUID"].(string)

	fmt.Print(uuid)
	createdObject, err := index.GetObject(uuid, nil)
	if err != nil {
		panic(err)
	}

	err = compareEvents(jsonStr, createdObject)
	if err != nil {
		panic(err)
	} else {
		fmt.Print("\ncreation : OK\n")
	}

	//UPDATE
	jsonStr = []byte(fmt.Sprintf(`{
			"UUID":	"%s", 
			"private": true,
			"sponsored": false,
			"title": "Running a Bordeaux",
			"startDate": "2019-04-16T06:10:18Z",
			"endDate": "2019-04-16T06:11:18Z",
			"description": "Course a pieds autour de Bordeaux",
			"latitude": 44.8404400,
			"longitude": -0.5805000,
			"sport": "86ff7f06-9432-4367-bbda-43e690453f6f",
			"minParticipants": 10,
			"maxParticipants": 24,
			"ownerUUID": "56af7f06-5192-4367-bbda-43e660463f6f"
		}`, uuid))
	req, err = http.NewRequest("PUT", Cfg.WeepiEventAddr, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")
	resp, err = httpClient.Do(req)
	if err != nil {
		panic(err)
	}
	fmt.Print("updated\n")

	time.Sleep(5 * time.Second)

	updatedObject, err := index.GetObject(uuid, nil)

	err = compareEvents(jsonStr, updatedObject)
	if err != nil {
		panic(err)
	} else {
		fmt.Print("update : OK\n")
	}

	//DELETE
	req, err = http.NewRequest("DELETE", fmt.Sprintf("%s/%s", Cfg.WeepiEventAddr, uuid), nil)
	resp, err = httpClient.Do(req)
	if err != nil {
		panic(err)
	}
}
