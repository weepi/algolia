FROM golang:alpine

MAINTAINER Florian Videau <florian.videau@epitech.eu>

ENV APP_PATH=$GOPATH/src/gitlab.com/weepi/algolia

ADD . $APP_PATH
WORKDIR $APP_PATH

RUN apk update
RUN apk add make git

RUN make install

ENTRYPOINT $GOPATH/bin/algolia
