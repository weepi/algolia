# Weepi algolia !

The weepi algolia microservice was used by [Weepen](https://weepen.io/) to manage the event search via [Algolia](https://www.algolia.com/).

The docker compose file in the [deploy repository](https://gitlab.com/weepi/deploy) was used to manage it.
