package main

import (
	"fmt"
	"gitlab.com/weepi/algolia/event"
	"time"

	"github.com/algolia/algoliasearch-client-go/algoliasearch"
)

func autoDeleteAlgoliaPastEvents(ac algoliasearch.Client) {

	ai := ac.InitIndex(Cfg.Env + "_events")

	ticker := time.NewTicker(time.Duration(Cfg.AlgoliaAutoDeleteEventIntervalHours) * time.Hour)

	for t := range ticker.C {
		Log.Infof("Algolia autodelete past event action triggered at: %v", t)

		timeNow := time.Now()
		_, err := ai.DeleteBy(algoliasearch.Map{
			"filters": fmt.Sprintf("endDateTimestamp < %d", timeNow.Unix()),
		})
		if err != nil {
			Log.With("error", err).Warnf("could not delete events after %s", timeNow.String())
			return
		}
	}
}

func registerEventMsgActions(clients clients) error {

	AlgoliaIndex := clients.AlgoliaClient.InitIndex(Cfg.Env + "_events")

	// Create
	if _, err := clients.NatsClient.Subscribe("algolia.event.create", func(subj, reply string, eventMsg event.EventMsg) {
		Log.Debugf("I saw a message in event.create: '%+v'", eventMsg)

		var errb error
		errb = event.Create(eventMsg, AlgoliaIndex, clients.MapsClient, clients.NatsClient)
		Log.Debugf("Event created in algolia", eventMsg)
		if err := clients.NatsClient.Publish(reply, &errb); err != nil {
			Log.With("error", err).Error("publishing error")
		}

	}); err != nil {
		Log.With("error", err).Error("event create subscribing failure")
		return err
	}

	// Update
	if _, err := clients.NatsClient.Subscribe("algolia.event.update", func(subj, reply string, eventMsg event.EventMsg) {
		Log.Debugf("I saw a message in event.update: '%+v'", eventMsg)

		var errb error
		errb = event.Update(eventMsg, AlgoliaIndex, clients.MapsClient, clients.NatsClient)
		Log.Debugf("Event updated in algolia", eventMsg)
		if err := clients.NatsClient.Publish(reply, &errb); err != nil {
			Log.With("error", err).Error("publishing error")
		}

	}); err != nil {
		Log.With("error", err).Error("event update subscribing failure")
		return err
	}

	// Join
	if _, err := clients.NatsClient.Subscribe("algolia.event.join", func(subj, reply string, uuid string) {
		Log.Debugf("I saw a message in event.join: '%+v'", uuid)

		var errb error
		errb = event.Join(uuid, AlgoliaIndex)
		Log.Debugf("Event joined in algolia", uuid)
		if err := clients.NatsClient.Publish(reply, &errb); err != nil {
			Log.With("error", err).Error("publishing error")
		}

	}); err != nil {
		Log.With("error", err).Error("event join subscribing failure")
		return err
	}

	// Leave
	if _, err := clients.NatsClient.Subscribe("algolia.event.leave", func(subj, reply string, uuid string) {
		Log.Debugf("I saw a message in event.leave: '%+v'", uuid)

		var errb error
		errb = event.Leave(uuid, AlgoliaIndex)
		Log.Debugf("Event leaved in algolia", uuid)
		if err := clients.NatsClient.Publish(reply, &errb); err != nil {
			Log.With("error", err).Error("publishing error")
		}

	}); err != nil {
		Log.With("error", err).Error("event leave subscribing failure")
		return err
	}

	// Delete
	if _, err := clients.NatsClient.Subscribe("algolia.event.delete", func(subj, reply string, eventMsg event.EventMsg) {
		Log.Debugf("I saw a message in event.delete: '%v'", eventMsg)

		var errb error
		errb = event.Delete(eventMsg, AlgoliaIndex)
		Log.Debugf("Event deleted in algolia", eventMsg)
		if err := clients.NatsClient.Publish(reply, &errb); err != nil {
			Log.With("error", err).Error("publishing error")
		}

	}); err != nil {
		Log.With("error", err).Error("event delete subscribing failure")
		return err
	}

	return nil
}
