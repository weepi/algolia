package main

import (
	"flag"
	"fmt"
	"os"
	"time"

	"gitlab.com/weepi/common/utils"

	"github.com/caarlos0/env"
	"github.com/joho/godotenv"
)

type config struct {
	NATSAddr                            string `env:"NATS_ADDR"`
	Env                                 string `env:"ENV"`
	AlgoliaAppId                        string `env:"ALGOLIA_APP_ID"`
	AlgoliaApiKey                       string `env:"ALGOLIA_API_KEY"`
	MapsApiKey                          string `env:"MAPS_API_KEY"`
	AlgoliaAutoDeleteEventIntervalHours int    `env:"ALGOLIA_AUTO_DELETE_EVENT_INTERVAL_HOURS" envDefault:"24"`
}

const (
	configFilename = "config"
	tagName        = "env"
)

var (
	Log *Logger
	Cfg config
)

func init() {
	var helpFlag = flag.Bool("help", false, "display help")
	flag.Parse()
	if *helpFlag {
		help()
		os.Exit(0)
	}

	var err error
	Log, err = NewLogger()
	if err != nil {
		panic(err)
	}
}

func help() {
	fmt.Println("The following env vars can be set:")
	for _, v := range utils.GetStructTags(Cfg, tagName) {
		fmt.Printf("%s\n", v)
	}
}

func main() {
	Log.Infof("service started at %s", time.Now())

	// Load env vars from file
	if err := godotenv.Load(configFilename); err != nil {
		Log.With("error", err).Warn("could not read configuration from file")
	}
	// Get config from env vars
	if err := env.Parse(&Cfg); err != nil {
		Log.With("error", err).Fatal("could not parse configuration")
	}

	clients := initClients()

	//NEEDEED for the service to work properly ;)
	//	res, _ := algoliaClient.GetLogs(algoliasearch.Map{"length": 0})
	//	fmt.Print(res)
	//

	registerEventMsgActions(clients)
	autoDeleteAlgoliaPastEvents(clients.AlgoliaClient)
}
